---
image: docker:latest

variables:
  APT_CACHE_DIR: "${CI_PROJECT_DIR}/.cache/apt"
  DOCKER_DRIVER: overlay
  DOCKER_BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  DOCKER_LATEST: $CI_REGISTRY_IMAGE:latest
  PIP_CACHE_DIR: "${CI_PROJECT_DIR}/.cache/pip"

stages:
  - test
  - branch
  - latest

before_script:
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY

.test: &test
  image: igwn/base:buster
  services:
    - postgres:12.5
    - memcached
  variables:
    AWS_SES_ACCESS_KEY_ID: "fake_aws_id"
    AWS_SES_SECRET_ACCESS_KEY: "fake_aws_key"
    DJANGO_ALERT_EMAIL_FROM: "fake_email"
    DJANGO_DB_HOST: "postgres"
    DJANGO_DB_PORT: "5432"
    DJANGO_DB_NAME: "fake_name"
    DJANGO_DB_USER: "runner"
    DJANGO_DB_PASSWORD: ""
    DJANGO_PRIMARY_FQDN: "fake_fqdn"
    DJANGO_SECRET_KEY: "fake_key"
    DJANGO_SETTINGS_MODULE: "config.settings.container.dev"
    DJANGO_TWILIO_ACCOUNT_SID: "fake_sid"
    DJANGO_TWILIO_AUTH_TOKEN: "fake_token"
    DJANGO_DOCKER_MEMCACHED_ADDR: "memcached:11211"
    ENABLE_LVALERT_OVERSEER: "false"
    ENABLE_IGWN_OVERSEER: "false"
    LVALERT_OVERSEER_PORT: "2"
    LVALERT_SERVER: "fake_server"
    LVALERT_USER: "fake_user"
    LVALERT_PASSWORD: "fake_password"
    ENABLE_IGWN_OVERSEER: "false"
    IGWN_ALERT_OVERSEER_PORT: "2"
    IGWN_ALERT_SERVER: "fake_server"
    IGWN_ALERT_USER: "fake_user"
    IGWN_ALERT_PASSWORD: "fake_password"
    POSTGRES_DB: "${DJANGO_DB_NAME}"
    POSTGRES_USER: "${DJANGO_DB_USER}"
    POSTGRES_PASSWORD: "${DJANGO_DB_PASSWORD}"
    POSTGRES_HOST_AUTH_METHOD: trust
  before_script:
    # create apt cache directory
    - mkdir -pv ${APT_CACHE_DIR}
    # set python version
    - PYTHON_VERSION="${CI_JOB_NAME##*:}"
    - PYTHON_MAJOR="${PYTHON_VERSION:0:1}"
    - PYTHON="python3"
    # install build requirements
    - sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    - wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
    - apt-get -yqq update
    - apt-get -o dir::cache::archives="${APT_CACHE_DIR}" install -yqq
          git
          libmariadbclient-dev
          libldap2-dev
          libsasl2-dev
          libssl-dev
          libxml2-dev
          krb5-user
          libkrb5-dev
          libsasl2-modules-gssapi-mit
          swig
          pkg-config 
          libpng-dev 
          libfreetype6-dev 
          libmariadb-dev-compat 
          libxslt-dev
          ${PYTHON}-pip
          postgresql-12 
          postgresql-client-12 
          libpq-dev
    # upgrade pip (requirement for lalsuite)
    - ${PYTHON} -m pip install --upgrade pip
    # install everything else from pip
    - ${PYTHON} -m pip install -r requirements.txt
    # create logs path required for tests
    - mkdir -pv ../logs/
    # list packages
    - ${PYTHON} -m pip list installed
  script:
    - PYTHONPATH=${PYTHONPATH}:${PWD}/gracedb ${PYTHON} -m pytest --cov-report term-missing --cov ./gracedb --junitxml=${CI_PROJECT_DIR}/junit.xml
  after_script:
    - rm -fvr ${PIP_CACHE_DIR}/log
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  artifacts:
    reports:
      junit: junit.xml
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .cache/pip
      - .cache/apt
  coverage: '/^TOTAL\s+.*\s+(\d+\.?\d*)%/'

test:3.7:
  <<: *test

branch_image:
  stage: branch
  script:
    - docker build --pull -t $DOCKER_BRANCH .
    - docker push $DOCKER_BRANCH
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure

latest_image:
  stage: latest
  dependencies:
    - branch_image
  script:
    - docker pull $DOCKER_BRANCH
    - docker tag $DOCKER_BRANCH $DOCKER_LATEST
    - docker push $DOCKER_LATEST
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
