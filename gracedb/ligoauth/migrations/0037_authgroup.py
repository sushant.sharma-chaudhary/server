# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-05-28 18:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0033_pipelinelog_and_pipeline_enabled'),
        ('auth', '0023_add_manage_pipeline_permissions'),
        ('ligoauth', '0036_add_cwb_cert'),
    ]

    operations = [
        migrations.CreateModel(
            name='AuthGroup',
            fields=[
                ('group_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='auth.Group')),
                ('description', models.TextField()),
                ('ldap_name', models.CharField(max_length=50, null=True, unique=True)),
                ('tag', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='events.Tag')),
            ],
            bases=('auth.group',),
        ),
    ]
