# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-27 19:57
from __future__ import unicode_literals

from django.db import migrations

# Creates custom table-level permissions for viewing events, adding events to
# specific pipelines, and t90-ing grbevents.

# Permission names, codenames, and corresponding content types
PERMISSIONS = [
    {
        'name': 'Can view event',
        'codename': 'view_event',
        'content_type': {'app': 'events', 'model': 'event'},
    },
    {
        'name': 'Can view grbevent',
        'codename': 'view_grbevent',
        'content_type': {'app': 'events', 'model': 'GrbEvent'},
    },
    {
        'name': 'Can view coincinspiralevent',
        'codename': 'view_coincinspiralevent',
        'content_type': {'app': 'events', 'model': 'CoincInspiralEvent'},
    },
    {
        'name': 'Can view multiburstevent',
        'codename': 'view_multiburstevent',
        'content_type': {'app': 'events', 'model': 'MultiBurstEvent'},
    },
    {
        'name': 'Can view siminspiral',
        'codename': 'view_siminspiralevent',
        'content_type': {'app': 'events', 'model': 'SimInspiralEvent'},
    },
    {
        'name': 'Can view lalinferenceburstevent',
        'codename': 'view_lalinferenceburstevent',
        'content_type': {'app': 'events', 'model': 'LalInferenceBurstEvent'},
    },
    {
        'name': 'Can populate pipeline',
        'codename': 'populate_pipeline',
        'content_type': {'app': 'events', 'model': 'Pipeline'},
    },
    {
        'name': 'Can t90 grbevent',
        'codename': 't90_grbevent',
        'content_type': {'app': 'events', 'model': 'GrbEvent'},
    },
]

def create_permissions(apps, schema_editor):
    Permission = apps.get_model('auth', 'Permission')
    ContentType = apps.get_model('contenttypes', 'ContentType')

    # Create permissions
    for perm_dict in PERMISSIONS:
        model = apps.get_model(perm_dict['content_type']['app'],
            perm_dict['content_type']['model'])
        ctype = ContentType.objects.get_for_model(model)
        perm, created = Permission.objects.get_or_create(name=perm_dict['name'],
            codename=perm_dict['codename'], content_type=ctype)

def delete_permissions(apps, schema_editor):
    Permission = apps.get_model('auth', 'Permission')
    ContentType = apps.get_model('contenttypes', 'ContentType')

    # Delete permissions
    for perm_dict in PERMISSIONS:
        try:
            ctype = ContentType.objects.get(app_label=perm_dict['content_type']['app'],
                model=perm_dict['content_type']['model'])
            perm = Permission.objects.get(name=perm_dict['name'],
                codename=perm_dict['codename'], content_type=ctype)
        except ContentType.DoesNotExist:
            print(("Error: can't get ContentType {0}.{1} to delete Permission "
                "{2}, skipping").format(perm_dict['content_type']['app'],
                perm_dict['content_type']['model'], perm_dict['codename']))
            break
        except Permission.DoesNotExist:
            print("Error: can't get Permission {0} to delete, skipping".format(
                perm_dict['codename']))
            break
            
class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
        ('auth', '0011_add_executives_group_permissions'),
    ]

    operations = [
        migrations.RunPython(create_permissions, delete_permissions)
    ]
