# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-05-08 16:27
# This migration adds permission for the emfollow user to
# populate the spiir, MBTAOnline, and pycbc pipelines. Requested
# by Roberto de Pietri, 10/21/2019

from __future__ import unicode_literals

from django.db import migrations

USER_NAME = 'emfollow'
PIPELINES = [
    'spiir',
    'MBTAOnline',
    'pycbc',
]


def add_permissions(apps, schema_editor):
    User = apps.get_model('auth', 'User')
    Permission = apps.get_model('auth', 'Permission')
    UserObjectPermission = apps.get_model('guardian', 'UserObjectPermission')
    Pipeline = apps.get_model('events', 'Pipeline')
    ContentType = apps.get_model('contenttypes', 'ContentType')

    # Get group
    em = User.objects.get(username=USER_NAME)

    perm = Permission.objects.get(codename='populate_pipeline')
    ctype = ContentType.objects.get_for_model(Pipeline)
    for pipeline in PIPELINES:
        pipeline = Pipeline.objects.get(name=pipeline)

        # Create UserObjectPermission
        gop = UserObjectPermission.objects.create(user=em,
            permission=perm, content_type=ctype, object_pk=pipeline.id)


def remove_permissions(apps, schema_editor):
    User = apps.get_model('auth', 'User')
    Permission = apps.get_model('auth', 'Permission')
    UserObjectPermission = apps.get_model('guardian', 'UserObjectPermission')
    Pipeline = apps.get_model('events', 'Pipeline')
    ContentType = apps.get_model('contenttypes', 'ContentType')

    # Get group
    em = User.objects.get(username=USER_NAME)

    perm = Permission.objects.get(codename='populate_pipeline')
    ctype = ContentType.objects.get_for_model(Pipeline)
    for pipeline in PIPELINES:
        pipeline = Pipeline.objects.get(name=pipeline)

        # Get UserObjectPermission and delete
        uop = UserObjectPermission.objects.get(user=em,
            permission=perm, content_type=ctype, object_pk=pipeline.id)
        uop.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ligoauth', '0005_update_emfollow_accounts'),
        ('guardian', '0005_authorize_raven_users_to_populate_pipelines'),
    ]

    operations = [
        migrations.RunPython(add_permissions, remove_permissions),
    ]
